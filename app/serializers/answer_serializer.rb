class AnswerSerializer < ApplicationSerializer

	attributes :id, :value

	belongs_to :question
	has_many :votes 
	has_many :comments
	belongs_to :user

	def value
		return object.value if object.status == "active"
	end

	def votes
		object.votes.select(:weight).group(:weight).count
	end
end