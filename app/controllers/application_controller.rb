class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  # include Response
  include Error::ErrorHandler
  before_action :validate_user?
  
  def validate_user?
    @current_user ||= User.find(params[:user_id])
  end

  def pagination(paginated_array, per_page)
    { pagination: { per_page: per_page.to_i,
                    total_pages: paginated_array.total_pages,
                    total_objects: paginated_array.total_entries } }
  end

end
