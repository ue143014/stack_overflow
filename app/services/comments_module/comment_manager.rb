module CommentsModule
	class CommentManager
		
		def initialize(params, current_user = nil)
			self.params = params
			self.current_user = current_user
		end

		def update_comment_details!
			return update_comment! if authenticate_user?
		end

		def remove_comments?
			return destroy_comment? if authenticate_user? 
		end

		private
		attr_accessor :params, :current_user

		def authenticate_user?
			@comment = Comment.find(params[:comment_id])
			return true if current_user.is_admin || (current_user.id == @comment.user_id)
		end

		def update_comment!
			@comment.value = params[:comment]
			@comment.save!
			@comment
		end

		def destroy_comment?
			return true if @comment.destroy!
		end

	end
end