module QuestionsModule

	class QuestionsManager
		
		def initialize(params, current_user=nil)
			self.params = params
			self.current_user = current_user
		end

		def find_questions!
			like_able_tags = params[:tags].map { |val| "%#{val.downcase}%" }
			tags = Tag.where("value ILIKE ANY ( array[?] )", like_able_tags).includes(:questions)
			questions = []
			tags.each do |tag_value|
				questions.push(tag_value.questions)
			end
			return questions.flatten
		end

		def update_question_details!
			return updated_question! if authenticated_user?
		end

		def remove_question?
			return destroy_question? if authenticated_user?
		end

		private
		attr_accessor :params, :current_user, :option 

		def authenticated_user?
			@question = Question.find(params[:question_id])
			return true if current_user.is_admin || (@question.user_id == current_user.id)
		end

		def updated_question!
			@question.value = params[:question]
			@question.save!
			@question
		end

		def destroy_question?
			return true if @question.destroy!
		end

	end
end