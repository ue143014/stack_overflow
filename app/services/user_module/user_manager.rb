module UserModule
	class UserManager

		def initialize(params, current_user)
			self.params = params
			self.current_user = current_user
		end

		def user_login!
			return @user if logged_in?
		end

		def update_user_details!
			return update_details! if validate? 
		end

		def remove_user?
			return destroy_user? if is_user_admin?
		end
		
		private
		attr_accessor :params, :current_user

		def logged_in?
			@user = User.find_by(email: params[:email].downcase )
			return true if @user && @user.authenticate(params[:password])
		end

		def validate?
			return true if current_user.email == params[:email].downcase
		end

		def update_details!
			current_user.name = params[:name]
			current_user.save!
			current_user
		end

		def is_user_admin?
			return true if current_user.is_admin
		end

		def destroy_user?
			return true if User.find(params[:remove_user_id]).destroy!
		end

	end

end