module TagsModule
	class TagsManager

		def initialize(params, current_user)
			self.params = params
			self.current_user = current_user
		end
		
		def remove_tags?
			return destroy_tags? if validate?
		end

		private
		attr_accessor :params, :current_user

		def validate?
			return true if current_user.is_admin?
		end

		def destory_tags?
			return true if Tag.where(value: params[:tags]).destroy_all
		end
	end
end